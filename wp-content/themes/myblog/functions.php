<?php
function theme_settup()
{
    register_nav_menu('main-menu', __('Menu chính'));
    add_theme_support('post-thumbnails');

    function setPostView($postID)
    {
        $countKey = 'views';
        $count = get_post_meta($postID, $countKey, true);
        if ($count == '') {
            $count = 0;
            delete_post_meta($postID, $countKey);
            add_post_meta($postID, $countKey, '0');
        } else {
            $count++;
            update_post_meta($postID, $countKey, $count);
        }
    }
    function getPostViews($postID)
    {
        $countKey = 'views';
        $count = get_post_meta($postID, $countKey, true);
        if ($count == '') {
            delete_post_meta($postID, $countKey);
            add_post_meta($postID, $countKey, '0');
            return "0";
        }
        return $count;
    }
}
add_action('init', 'theme_settup');
