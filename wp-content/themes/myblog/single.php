<?php get_header() ?>
<div id="content">
    <div class="container">
        <div class="break">
            <?php if (function_exists('yoast_breadcrumb')) {
                yoast_breadcrumb('<p id="breadcrumbs"><i class="fa fa-home"></i> ', ' </p>');
            } ?>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9">
                <div class="post-news">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) :
                            the_post();
                            setPostView(get_the_ID())
                    ?>
                            <h1 class="single-title">
                                <?php the_title() ?>
                            </h1>
                            <div class="meta">
                                <span>Ngày đăng: <?= get_the_date(); ?></span>
                                <span>Tác giả: <?= get_the_author(); ?></span>
                                <span>Chuyên mục: <?= the_category(','); ?></span>
                                <span>Lượt xem: <?= getPostViews(get_the_ID()) ?> lượt</span>
                            </div>
                            <article class="post-content">
                                <?= the_content() ?>
                            </article>
                            <div class="tag">
                                <p><?= the_tags('từ khóa: ') ?></p>
                            </div>
                            <div class="lienquan">
                                <h3>Bài viết liên quan</h3>
                                <div class="content-lienquan">
                                    <?php
                                    $categories = get_the_category(get_the_ID());
                                    if ($categories) :
                                        $category_ids = array();
                                        foreach ($categories as $individual_category) {
                                            $category_ids[] = $individual_category->term_id;
                                        }
                                        $args = array(
                                            'category__in' => $category_ids,
                                            'post__no_in' => array(get_the_ID()),
                                            'showposts' => 4,
                                        );
                                        $my_query = new WP_Query($args);
                                        if ($my_query->have_posts()) :
                                    ?>
                                            <ul class="list-news-lq">
                                                <?php
                                                while ($my_query->have_posts()) :
                                                    $my_query->the_post();
                                                ?>
                                                    <li>
                                                        <div class="new-img">
                                                            <a href="<?= the_permalink() ?>"><?= the_post_thumbnail(array(85, 75)) ?></a>
                                                        </div>
                                                        <div class="item-list">
                                                            <h4>
                                                                <a href="<?= the_permalink() ?>"><?= the_title() ?></a>
                                                                <div class="meta-link">
                                                                    <span>Ngày đăng: <?= get_the_date() ?> </span>
                                                                </div>
                                                            </h4>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </li>
                                                <?php
                                                endwhile; ?>
                                            </ul>
                                    <?php
                                        endif;
                                    endif;
                                    ?>
                                </div>
                            </div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>