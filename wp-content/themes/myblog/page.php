<?php get_header() ?>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9">
                <div class="post-news">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) :
                            the_post();
                            setPostView(get_the_ID())
                    ?>
                            <h1 class="single-title">
                                <?php the_title() ?>
                            </h1>
                            <article class="post-content">
                                <?= the_content() ?>
                            </article>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>