<div class="noibat">
    <h2 class="title-news">Bài viết nổi bật</h2>
    <?php
    $args = array(
        'post_status' => 'publish', //only get posts have been published
        'post_type' => 'post', //lấy những bài viết thuộc post, nếu lấy những bài trong trang thì đê 'page'
        'showposts' => 1, //số lượng bài viết
        'cat' => 7 //lấy bài viết trong chuyện mục có id là 1
    );
    ?>
    <?php
    $get_posts = new WP_query($args);
    global $wp_query;
    $wp_query->in_the_loop = true;
    ?>
    <?php while ($get_posts->have_posts()) : $get_posts->the_post(); ?>
        <div class="content-nb">
            <a href="<?php the_permalink(); ?>">
                <?= get_the_post_thumbnail(get_the_id(), 'full', array('class' => 'thumbnail')); ?>
            </a>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <div class="meta">
                <span>ngày đăng: <?= get_the_date(); ?></span>
                <span>Lượt xem: <?= getPostViews(get_the_ID()) ?> lượt</span>
            </div>
            <?php the_excerpt(); ?>
        </div>
    <?php endwhile;
    wp_reset_postdata() ?>
    <div class="list-nb">
        <div class="row">
            <?php
            $args = array(
                'post_status' => 'publish', //only get posts have been published
                'post_type' => 'post', //lấy những bài viết thuộc post, nếu lấy những bài trong trang thì đê 'page'
                'showposts' => 3, //số lượng bài viết
                'cat' => 7, //lấy bài viết trong chuyện mục có id là 1,
                'offset' => 1 //bỏ đi 1 bài viết đầu tiên
            );
            $get_posts = new WP_query($args);
            global $wp_query;
            $wp_query->in_the_loop = true;
            ?>
            <?php while ($get_posts->have_posts()) : $get_posts->the_post(); ?>
                <div class="col-xs-4 col-sm-4 col-md-4 style-box">
                    <div class="list-post">
                        <a href="<?php the_permalink() ?>">
                            <?= get_the_post_thumbnail(get_the_id(), 'full', array('class' => 'thumbnail')); ?>
                        </a>
                        <h4>
                            <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                        </h4>
                        <div class="meta">
                            <span>ngày đăng: <?= get_the_date(); ?></span>
                            <span>Lượt xem: <?= getPostViews(get_the_ID()) ?> lượt</span>
                        </div>
                    </div>
                </div>
            <?php endwhile;
            wp_reset_postdata() ?>
        </div>
    </div>
</div>