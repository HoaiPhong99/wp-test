<div class="widget">
    <h3>Tìm kiếm</h3>
    <div class="content-search">
        <form action="<?php bloginfo( 'url' ) ?>/" method="get">
            <input type="text" name="s" id="" class="form-control" placeholder="từ khóa...">
            <button type="submit" class="btn btn-primary">tìm kiếm</button>
        </form>
    </div>
</div>
<div class="widget">
    <h3>Chuyên mục</h3>
    <div class="content-w">
        <ul>
            <?php
            $args = array(
                'child_of' => 0,
                'orderBy' => 'id'
            );
            $categories = get_categories($args);
            foreach ( $categories as $category) :            
            ?>
                <li>
                    <a href="<?= get_term_link( $category->slug, 'category' ) ?>">
                        <?= $category->name ?>
                        <span>(<?= $category->count ?>)</span>
                    </a>
                </li>
            <? endforeach; ?>

        </ul>
    </div>
</div>
<div class="widget">
    <h3>Bài viết mới</h3>
    <div class="content-new">
        <ul>
        <?php
            $get_posts = new WP_Query();
            $get_posts->query('post_type=publish&showposts=5&post_type=post');
            global $wp_query;
            $wp_query->in_the_loop = true;
            while ($get_posts->have_posts(  )):
                $get_posts->the_post();
    ?>
            <li>
                <a href="<?= the_permalink() ?>">
                    <?= get_the_post_thumbnail( get_the_ID(  ), 'full', array('class' => 'thumbnail') ) ?>
                </a>
                <h4><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h4>
                <div class="meta"><span>Ngày đăng: <?= get_the_date() ?></span></div>
                <div class="clear"></div>
            </li>
        <?php
            endwhile;
            wp_reset_postdata(  );
        ?>
        </ul>
    </div>
</div>
<div class="widget">
    <h3>Bài viết xem nhiều</h3>
    <div class="content-mostv">
        <ul>
        <?php 
        $i = 1;
        $get_posts = new WP_Query();
        $get_posts->query('post_status=publish&showposts=8&post_type=post&meta_key=views&orderby=meta_value_num');
        global $wp_query; $wp_query->in_the_loop = true;
        while($get_posts->have_posts(  )) : $get_posts->the_post(  );
        ?>
            <li>
                <span><?= $i ?></span>
                <h4><a href="<?= the_permalink( ) ?>"><?= the_title() ?></a></h4>
                <div class="clear"></div>
            </li>
            <?php $i++ ?>
        <?php endwhile; wp_reset_postdata(  );?>
        </ul>
    </div>
</div>
<div class="widget">
    <h3>Quảng cáo</h3>
    <div class="content-wc">
        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/qc.jpg" alt=""></a>
    </div>
</div>
<div class="widget">
    <h3>Bạn bè</h3>
    <div class="content-w">
        <ul>
            <li><a href="http://huykira.net">Huy Kira</a></li>
            <li><a href="http://huykira.net">Blog Huy Kira</a></li>
        </ul>
    </div>
</div>
<div class="widget">
    <h3>Facebook</h3>
    <div class="content-fb">
        <ul>
            <li><a href="http://huykira.net"><img src="<?php bloginfo('template_directory'); ?>/images/facebook.jpg" alt=""></a></li>
        </ul>
    </div>
</div>